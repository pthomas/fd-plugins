# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:04+0000\n"
"PO-Revision-Date: 2018-08-13 19:52+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: German (https://www.transifex.com/fusiondirectory/teams/12202/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: addons/debugHelp/class_debugHelp.inc:27
#: addons/debugHelp/class_debugHelp.inc:33
msgid "Debug help"
msgstr "Debug-Hilfe"

#: addons/debugHelp/class_debugHelp.inc:28
msgid "Debug help tools"
msgstr "Debug-Hilfewerkzeuge"

#: addons/debugHelp/class_debugHelp.inc:45
msgid "Diagrams"
msgstr "Diagramme"

#: addons/debugHelp/class_debugHelp.inc:49
msgid "Object types diagram"
msgstr "Objektartendiagramm"

#: addons/debugHelp/class_debugHelp.inc:51
msgid "Get"
msgstr "Erhalten"

#: addons/debugHelp/class_debugHelp.inc:56
msgid "Object Types"
msgstr "Objekttypen"

#: addons/debugHelp/debughelp.tpl.c:2
msgid "Insufficient rights"
msgstr "Nicht ausreichende Rechte"
