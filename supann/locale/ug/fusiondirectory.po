# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:05+0000\n"
"PO-Revision-Date: 2018-08-13 20:04+0000\n"
"Language-Team: Uighur (https://www.transifex.com/fusiondirectory/teams/12202/ug/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ug\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: admin/supannStructures/class_etablissement.inc:27
#: personal/supann/class_supannAccount.inc:336
#: personal/supann/class_supannAccount.inc:375
msgid "Establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:28
msgid "SupAnn Establishment Settings"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:32
msgid "SupAnn Establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:49
msgid "Properties"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:52
msgid "Root establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:52
msgid "Set this establishment as the root one"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:56
#: admin/supannStructures/class_entite.inc:51
msgid "Name"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:56
msgid "The name to write in the o attribute for this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:60
#: admin/supannStructures/class_entite.inc:55
msgid "Description"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:60
msgid "A short description of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:66
#: admin/supannStructures/class_etablissement.inc:77
msgid "Location"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:69
#: admin/supannStructures/class_entite.inc:64
msgid "Telephone"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:69
msgid "Phone number of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:73
#: admin/supannStructures/class_entite.inc:68
msgid "Fax"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:73
msgid "Fax number of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:77
msgid "Usually the city where this establishment is"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:81
msgid "Address"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:81
msgid "The postal address of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:87
msgid "SupAnn properties"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:90
msgid "Establishment code"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:90
msgid "The code of this establishment (must have a prefix between {})"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:95
msgid "Establishment type"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:95
msgid "The SupAnn type that best fits this Establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:99
msgid "SupAnn code"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:99
msgid "The SupAnn code for this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:105
#: admin/supannStructures/class_entite.inc:91
msgid "Parent entities"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:105
#: admin/supannStructures/class_entite.inc:91
msgid "The parent entities of this entity"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:111
#: admin/supannStructures/class_entite.inc:97
msgid "Reference IDs"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:111
#: admin/supannStructures/class_entite.inc:97
msgid "supannRefId - IDs/links for this entity on other systems"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:119
msgid "Legal name"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:119
msgid "The legal name of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:123
msgid "Home page URI"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:123
msgid "The URI of this establishment website home page"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:127
msgid "Institution URI"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:127
msgid "The URI of this establishment institution website"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:131
msgid "White pages URI"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:131
msgid "The URI of this establishment white pages"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:177
#: admin/supannStructures/class_etablissement.inc:250
msgid "LDAP error"
msgstr ""

#: admin/supannStructures/class_supannStructuresManagement.inc:38
msgid "SupAnn structures"
msgstr ""

#: admin/supannStructures/class_supannStructuresManagement.inc:39
msgid "SupAnn structures management"
msgstr ""

#: admin/supannStructures/class_supann.inc:53
msgid "File error"
msgstr ""

#: admin/supannStructures/class_supann.inc:54
#, php-format
msgid "Cannot read file: '%s'"
msgstr ""

#: admin/supannStructures/class_entite.inc:27
#: admin/supannStructures/class_entite.inc:48
#: personal/supann/class_supannAccount.inc:464
msgid "Entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:28
msgid "SupAnn Entity Settings"
msgstr ""

#: admin/supannStructures/class_entite.inc:31
msgid "SupAnn Entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:51
msgid "The name to write in the ou attribute for this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:55
msgid "Short description of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:61
msgid "Administrative information"
msgstr ""

#: admin/supannStructures/class_entite.inc:64
msgid "Phone number of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:68
msgid "Fax number of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:72
msgid "Postal address"
msgstr ""

#: admin/supannStructures/class_entite.inc:72
msgid "Postal address of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:78
msgid "SupAnn information"
msgstr ""

#: admin/supannStructures/class_entite.inc:81
#: personal/supann/class_supannAccount.inc:460
msgid "Entity type"
msgstr ""

#: admin/supannStructures/class_entite.inc:81
msgid "The SupAnn type that best fits this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:85
msgid "Entity code"
msgstr ""

#: admin/supannStructures/class_entite.inc:85
msgid "The SupAnn code of this entity"
msgstr ""

#: config/supann/class_supannConfig.inc:29
#: config/supann/class_supannConfig.inc:51
#: personal/supann/class_supannAccount.inc:223
msgid "SupAnn"
msgstr ""

#: config/supann/class_supannConfig.inc:30
msgid "SupAnn configuration"
msgstr ""

#: config/supann/class_supannConfig.inc:31
msgid "FusionDirectory SupAnn plugin configuration"
msgstr ""

#: config/supann/class_supannConfig.inc:55
msgid "SupAnn RDN"
msgstr ""

#: config/supann/class_supannConfig.inc:55
msgid "Branch in which SupAnn structures will be stored"
msgstr ""

#: config/supann/class_supannConfig.inc:60
msgid "SupAnn mail for recovery"
msgstr ""

#: config/supann/class_supannConfig.inc:60
msgid ""
"Allow the use of mail addresses from the personal mail address field from "
"SupAnn account for password recovery"
msgstr ""

#: config/supann/class_supannConfig.inc:66
msgid "Custom resources and their labels"
msgstr ""

#: config/supann/class_supannConfig.inc:70
#: personal/supann/class_supannAccountStatus.inc:59
#: personal/supann/class_supannAccountStatus.inc:124
msgid "Resource"
msgstr ""

#: config/supann/class_supannConfig.inc:70
msgid "String that will be stored between {} in supannRessourceEtatDate"
msgstr ""

#: config/supann/class_supannConfig.inc:74
#: config/supann/class_supannConfig.inc:119
msgid "Label"
msgstr ""

#: config/supann/class_supannConfig.inc:74
msgid "Label shown for this resource"
msgstr ""

#: config/supann/class_supannConfig.inc:80
msgid "Custom resources"
msgstr ""

#: config/supann/class_supannConfig.inc:88
msgid "Allowed substates for an account"
msgstr ""

#: config/supann/class_supannConfig.inc:92
#: personal/supann/class_supannAccountStatus.inc:51
#: personal/supann/class_supannAccountStatus.inc:65
#: personal/supann/class_supannAccountStatus.inc:125
msgid "Status"
msgstr ""

#: config/supann/class_supannConfig.inc:92
msgid "Active status this substatus is tied to"
msgstr ""

#: config/supann/class_supannConfig.inc:95
#: personal/supann/class_supannAccountStatus.inc:68
#: personal/supann/class_supannAccountStatus.inc:243
msgid "Active"
msgstr ""

#: config/supann/class_supannConfig.inc:95
#: personal/supann/class_supannAccountStatus.inc:68
#: personal/supann/class_supannAccountStatus.inc:250
msgid "Inactive"
msgstr ""

#: config/supann/class_supannConfig.inc:95
#: personal/supann/class_supannAccountStatus.inc:68
msgid "Suspended"
msgstr ""

#: config/supann/class_supannConfig.inc:98
#: config/supann/class_supannConfig.inc:115
#: personal/supann/class_supannAccountStatus.inc:71
#: personal/supann/class_supannAccountStatus.inc:126
msgid "Substatus"
msgstr ""

#: config/supann/class_supannConfig.inc:98
#: config/supann/class_supannConfig.inc:115
msgid "LDAP value for this substatus"
msgstr ""

#: config/supann/class_supannConfig.inc:104
msgid "Substates"
msgstr ""

#: config/supann/class_supannConfig.inc:111
msgid "Labels for custom substates"
msgstr ""

#: config/supann/class_supannConfig.inc:119
msgid "Label shown for this substatus"
msgstr ""

#: config/supann/class_supannConfig.inc:125
msgid "Custom labels"
msgstr ""

#: personal/supann/class_supannAccount.inc:37
#: personal/supann/class_supannAccount.inc:120
msgid "None"
msgstr ""

#: personal/supann/class_supannAccount.inc:117
msgid "Licence"
msgstr ""

#: personal/supann/class_supannAccount.inc:117
msgid "Master"
msgstr ""

#: personal/supann/class_supannAccount.inc:117
msgid "Ph.D."
msgstr ""

#: personal/supann/class_supannAccount.inc:117
msgid "Another class of degree"
msgstr ""

#: personal/supann/class_supannAccount.inc:117
msgid "Post-graduate year"
msgstr ""

#: personal/supann/class_supannAccount.inc:120
msgid "1st year"
msgstr ""

#: personal/supann/class_supannAccount.inc:120
msgid "2nd year"
msgstr ""

#: personal/supann/class_supannAccount.inc:120
msgid "3rd year"
msgstr ""

#: personal/supann/class_supannAccount.inc:121
msgid "4th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:121
msgid "5th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:121
msgid "6th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:122
msgid "7th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:122
msgid "8th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:122
msgid "9th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:224
msgid "SupAnn information management plugin"
msgstr ""

#: personal/supann/class_supannAccount.inc:243
msgid "Identity"
msgstr ""

#: personal/supann/class_supannAccount.inc:246
msgid "Civilite"
msgstr ""

#: personal/supann/class_supannAccount.inc:246
msgid "supannCivilite - Civility for this person"
msgstr ""

#: personal/supann/class_supannAccount.inc:251
msgid "Alias login"
msgstr ""

#: personal/supann/class_supannAccount.inc:251
msgid "supannAliasLogin - An alias for the login of this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:255
msgid "eduPersonPrincipalName"
msgstr ""

#: personal/supann/class_supannAccount.inc:255
msgid ""
"eduPersonPrincipalName - A name that looks like <id>@<domain> which is "
"unique for this domain, and has not be assigned to anyone else recently"
msgstr ""

#: personal/supann/class_supannAccount.inc:259
msgid "Nickname"
msgstr ""

#: personal/supann/class_supannAccount.inc:259
msgid "eduPersonNickname - Can contain a nickname for this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:264
msgid "Ref ids"
msgstr ""

#: personal/supann/class_supannAccount.inc:264
msgid "supannRefId - IDs/links for this user on other systems"
msgstr ""

#: personal/supann/class_supannAccount.inc:274
msgid "Contact"
msgstr ""

#: personal/supann/class_supannAccount.inc:278
msgid "Other phone numbers"
msgstr ""

#: personal/supann/class_supannAccount.inc:278
msgid "supannAutreTelephone - Other phone numbers for this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:284
msgid "Other mail addresses"
msgstr ""

#: personal/supann/class_supannAccount.inc:284
msgid ""
"supannAutreMail - Other mail addresses for this user. Each must be unique"
msgstr ""

#: personal/supann/class_supannAccount.inc:290
msgid "Personal mail addresses"
msgstr ""

#: personal/supann/class_supannAccount.inc:290
msgid "supannMailPerso - Personal mail addresses for this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:295
msgid "Red list"
msgstr ""

#: personal/supann/class_supannAccount.inc:295
msgid "supannListeRouge - Should this person be on the red list"
msgstr ""

#: personal/supann/class_supannAccount.inc:301
msgid "Assignment"
msgstr ""

#: personal/supann/class_supannAccount.inc:304
msgid "Primary assignment"
msgstr ""

#: personal/supann/class_supannAccount.inc:304
msgid "supannEntiteAffectationPrincipale - Main assignment of the person"
msgstr ""

#: personal/supann/class_supannAccount.inc:309
msgid "Assignments"
msgstr ""

#: personal/supann/class_supannAccount.inc:309
#: personal/supann/class_supannAccount.inc:464
msgid ""
"supannEntiteAffectation - Represents assignments of the person in an "
"institution, a component, service, etc."
msgstr ""

#: personal/supann/class_supannAccount.inc:315
msgid "Entity types"
msgstr ""

#: personal/supann/class_supannAccount.inc:315
msgid ""
"supannTypeEntiteAffectation - Types of the entities this person is assigned "
"to"
msgstr ""

#: personal/supann/class_supannAccount.inc:322
msgid "Affiliation"
msgstr ""

#: personal/supann/class_supannAccount.inc:325
msgid "Primary affiliation"
msgstr ""

#: personal/supann/class_supannAccount.inc:325
msgid "eduPersonPrimaryAffiliation - Main status of the person"
msgstr ""

#: personal/supann/class_supannAccount.inc:330
msgid "Affiliations"
msgstr ""

#: personal/supann/class_supannAccount.inc:330
msgid ""
"eduPersonAffiliation - Status of the person: student, BIATOSS, teacher, "
"contract, retired, hosted staff (CNRS, INSERM, etc.), a former student, etc."
msgstr ""

#: personal/supann/class_supannAccount.inc:336
msgid ""
"supannEtablissement - Institution or unit of administrative attachment of "
"the person"
msgstr ""

#: personal/supann/class_supannAccount.inc:343
msgid "Student profile"
msgstr ""

#: personal/supann/class_supannAccount.inc:346
msgid "INE code"
msgstr ""

#: personal/supann/class_supannAccount.inc:346
msgid "supannCodeINE - INE code of this student"
msgstr ""

#: personal/supann/class_supannAccount.inc:350
msgid "Student ID"
msgstr ""

#: personal/supann/class_supannAccount.inc:350
msgid "supannEtuId - Scolarity id"
msgstr ""

#: personal/supann/class_supannAccount.inc:356
msgid "Student registrations"
msgstr ""

#: personal/supann/class_supannAccount.inc:371
msgid "supannEtuInscription - Registrations for this student"
msgstr ""

#: personal/supann/class_supannAccount.inc:375
msgid ""
"supannEtablissement - Etablissement in which this registration was done"
msgstr ""

#: personal/supann/class_supannAccount.inc:379
msgid "Year"
msgstr ""

#: personal/supann/class_supannAccount.inc:379
msgid "supannEtuAnneeInscription - The year this registration will begin"
msgstr ""

#: personal/supann/class_supannAccount.inc:384
msgid "Registration type"
msgstr ""

#: personal/supann/class_supannAccount.inc:384
msgid "supannEtuRegimeInscription - The type of this registration"
msgstr ""

#: personal/supann/class_supannAccount.inc:388
msgid "Disciplinary Sector"
msgstr ""

#: personal/supann/class_supannAccount.inc:388
msgid "supannEtuSecteurDisciplinaire - Disciplinary sector education diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:392
msgid "Diploma type"
msgstr ""

#: personal/supann/class_supannAccount.inc:392
msgid "supannEtuTypeDiplome - Type of diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:396
msgid "Curriculum year "
msgstr ""

#: personal/supann/class_supannAccount.inc:396
msgid ""
"supannEtuCursusAnnee - Type of curriculum (L, M, D or X, ...) and the year "
"in the diploma."
msgstr ""

#: personal/supann/class_supannAccount.inc:400
msgid "Entity assignment"
msgstr ""

#: personal/supann/class_supannAccount.inc:400
msgid "supannEntiteAffectation - To wich entities does this user belong to"
msgstr ""

#: personal/supann/class_supannAccount.inc:404
msgid "Diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:404
msgid "supannEtuDiplome - Diploma prepared by the student"
msgstr ""

#: personal/supann/class_supannAccount.inc:408
msgid "Step"
msgstr ""

#: personal/supann/class_supannAccount.inc:408
msgid ""
"supannEtuEtape - Step can be considered a split (semester, year, etc.) in "
"time of education leading to a diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:412
msgid "educational element"
msgstr ""

#: personal/supann/class_supannAccount.inc:412
msgid ""
"supannEtuElementPedagogique - Generic description of the content of "
"education with a high level of granularity"
msgstr ""

#: personal/supann/class_supannAccount.inc:426
msgid "Personal profile"
msgstr ""

#: personal/supann/class_supannAccount.inc:429
msgid "Personal ID"
msgstr ""

#: personal/supann/class_supannAccount.inc:429
msgid "supannEmpId - Employee identifier"
msgstr ""

#: personal/supann/class_supannAccount.inc:433
msgid "Personal corps"
msgstr ""

#: personal/supann/class_supannAccount.inc:433
msgid "supannEmpCorps"
msgstr ""

#: personal/supann/class_supannAccount.inc:438
msgid "Activity"
msgstr ""

#: personal/supann/class_supannAccount.inc:438
msgid "supannActivite - Category of profession"
msgstr ""

#: personal/supann/class_supannAccount.inc:445
msgid "Roles"
msgstr ""

#: personal/supann/class_supannAccount.inc:452
msgid "supannRoleEntite"
msgstr ""

#: personal/supann/class_supannAccount.inc:456
msgid "Generic role"
msgstr ""

#: personal/supann/class_supannAccount.inc:456
msgid "supannRoleGenerique - Generic role of the person in the facility"
msgstr ""

#: personal/supann/class_supannAccount.inc:460
msgid "supannTypeEntiteAffectation - type of the assigned entity"
msgstr ""

#: personal/supann/class_supannAccount.inc:575
msgid ""
"\"member\" and \"affiliate\" values are incompatible for "
"eduPersonAffiliation. Please remove one of them."
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:31
msgid "SupAnn status"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:32
msgid "SupAnn status management"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:55
msgid "State of the account and associated resources"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:59
msgid "Which resource this state concerns"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:62
#: personal/supann/class_supannAccountStatus.inc:111
msgid "Account"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:62
#: personal/supann/class_supannAccountStatus.inc:111
msgid "Mail"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:65
msgid "Active status"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:76
msgid "Start date"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:76
msgid "Date this status started"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:82
msgid "End date"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:82
msgid "Date this status will end"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:127
msgid "Begin"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:128
msgid "End"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:186
#, php-format
msgid "Label \"%s\" is present more than once in supannRessourceEtatDate"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:242
msgid "Anticipated"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:244
msgid "Extension"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:247
msgid "Precreated"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:248
msgid "Created"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:249
msgid "Expired"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:251
msgid "Data deletion"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:252
msgid "Account deletion"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:255
msgid "Locked"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:256
msgid "Administrative lock"
msgstr ""

#: personal/supann/class_supannAccountStatus.inc:257
msgid "Technical lock"
msgstr ""
