<body onLoad="javascript:$$('div.debug_div').each(function (a) { a.hide(); });">
  {* FusionDirectory public form - smarty template *}
  {$php_errors}
  {$msg_dialogs}

  <div id="window-container">

    <div id="window-div">
      <form name="mainform" id="mainform" method="post" enctype="multipart/form-data">
        <div id="window-titlebar">
          <img id="fd-logo" src="geticon.php?context=applications&amp;icon=fusiondirectory&amp;size=48" alt="FusionDirectory"/>
          <p>
            {$form.fdPublicFormTitle|escape}
          </p>
        </div>
        <div id="window-content">
          <div>
            {if $done}
              <p class="infotext">
                {if $errorMessage}
                  {$errorMessage|escape}
                {else}
                  {$form.fdPublicFormFinalText|escape}
                {/if}
              </p>
            {else}
              <p class="infotext">
                {$form.fdPublicFormText|escape}
              </p>

              <br/>
              {$template_dialog}
            {/if}
          </div>
          {if (!$done and $form.fdPublicFormTosUrl)}
            <label for="tosCheckBox"><input type="checkbox" id="tosCheckBox" name="tosCheckBox"/> {t 1=$form.fdPublicFormTosUrl escape=no}I agree to the <a target="_blank" href="%1">terms of service</a>{/t}</label>
          {/if}
        </div>
        {if !$done}
        <div id="window-footer" class="plugbottom">
          <div>
          </div>
          <div>
            <input type="submit" id="form_submit" name="form_submit" value="{msgPool type=okButton}"/>
          </div>
        </div>
        {/if}
        <input type="hidden" name="CSRFtoken" value="{$CSRFtoken}"/>
      </form>
    </div>
  </div>
  <script type="text/javascript">
    <!-- // Error Popup
    next_msg_dialog();
    -->
  </script>
</body>
</html>
