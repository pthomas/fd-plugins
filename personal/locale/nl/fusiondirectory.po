# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Rombout de Neef <rombout.deneef@digipolis.be>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:05+0000\n"
"PO-Revision-Date: 2018-08-13 19:59+0000\n"
"Last-Translator: Rombout de Neef <rombout.deneef@digipolis.be>, 2018\n"
"Language-Team: Dutch (https://www.transifex.com/fusiondirectory/teams/12202/nl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/personal/class_personalConfig.inc:26
msgid "Personal configuration"
msgstr "Persoonlijke configuratie"

#: config/personal/class_personalConfig.inc:27
msgid "FusionDirectory personal plugin configuration"
msgstr "FusionDirectory persoonlijke pluginconfiguratie"

#: config/personal/class_personalConfig.inc:40
#: personal/personal/class_personalInfo.inc:84
msgid "Personal"
msgstr "Persoonlijk"

#: config/personal/class_personalConfig.inc:43
msgid "Allow use of private email for password recovery"
msgstr "Laat het gebruik van private e-mail voor paswoordherstel toe"

#: config/personal/class_personalConfig.inc:43
msgid "Allow users to use their private email address for password recovery"
msgstr ""
"Laat gebruikers toe om hun privé-e-mailadres te gebruiken voor herstel van "
"hun paswoord"

#: personal/personal/class_personalInfo.inc:30
msgid "Site"
msgstr "Site"

#: personal/personal/class_personalInfo.inc:30
msgid "Website the account is on"
msgstr "Website waarop het account zit"

#: personal/personal/class_personalInfo.inc:36
msgid "Id"
msgstr "Id"

#: personal/personal/class_personalInfo.inc:36
msgid "Id of this user on this website"
msgstr "Id van deze gebruiker op de website"

#: personal/personal/class_personalInfo.inc:85
msgid "Personal information"
msgstr "Persoonlijke informatie"

#: personal/personal/class_personalInfo.inc:102
msgid "Personal info"
msgstr "Persoonlijke info"

#: personal/personal/class_personalInfo.inc:105
msgid "Personal title"
msgstr "Aanhef"

#: personal/personal/class_personalInfo.inc:105
msgid "Personal title - Examples of personal titles are \"Ms\", \"Dr\", \"Prof\" and \"Rev\""
msgstr ""
"Persoonlijke titel - voorbeelden van persoonlijke titels zijn \"Mevr\", "
"\"Dr\", \"Prof\" en \"Rev\""

#: personal/personal/class_personalInfo.inc:110
msgid "Nickname"
msgstr "Roepnaam"

#: personal/personal/class_personalInfo.inc:110
msgid "Nicknames for this user"
msgstr "Roepnaam van deze gebruiker"

#: personal/personal/class_personalInfo.inc:115
msgid "Badge Number"
msgstr "Badgenummer"

#: personal/personal/class_personalInfo.inc:115
msgid "Company badge number"
msgstr "Bedrijfsbadgenummer"

#: personal/personal/class_personalInfo.inc:119
msgid "Date of birth"
msgstr "Geboortedatum"

#: personal/personal/class_personalInfo.inc:126
msgid "Sex"
msgstr "Geslacht"

#: personal/personal/class_personalInfo.inc:126
msgid "Gender"
msgstr "Geslacht"

#: personal/personal/class_personalInfo.inc:131
msgid "Country"
msgstr "Land"

#: personal/personal/class_personalInfo.inc:135
msgid "Start date"
msgstr "Startdatum"

#: personal/personal/class_personalInfo.inc:135
msgid "Date this user joined the company"
msgstr "Datum waarop deze gebruiker bij het bedrijf in dienst trad"

#: personal/personal/class_personalInfo.inc:140
msgid "End date"
msgstr "Einddatum"

#: personal/personal/class_personalInfo.inc:140
msgid "Date this user is supposed to leave the company"
msgstr ""
"Datum waarop deze gebruiker verondersteld wordt het bedrijf te verlaten"

#: personal/personal/class_personalInfo.inc:145
msgid "Photo Visible"
msgstr "Foto  zichtbaar"

#: personal/personal/class_personalInfo.inc:145
msgid "Should the photo of the user be visible on external tools"
msgstr "Moet de foto van de gebruiker zichtbaar zijn op externe tools"

#: personal/personal/class_personalInfo.inc:151
msgid "Contact"
msgstr "Contact"

#: personal/personal/class_personalInfo.inc:155
msgid "Social account"
msgstr "Sociaal account"

#: personal/personal/class_personalInfo.inc:155
msgid "Social accounts of this user"
msgstr "Sociale accounts van deze gebruiker"

#: personal/personal/class_personalInfo.inc:162
msgid "Private email"
msgstr "Privé-e-mail"

#: personal/personal/class_personalInfo.inc:162
msgid "Private email addresses of this user"
msgstr "Privé-e-mailadres voor deze gebruiker"

#: personal/personal/class_socialHandlers.inc:68
msgid "Facebook"
msgstr "Facebook"

#: personal/personal/class_socialHandlers.inc:78
msgid "Twitter"
msgstr "Twitter"

#: personal/personal/class_socialHandlers.inc:98
msgid "Google+"
msgstr "Google+"

#: personal/personal/class_socialHandlers.inc:118
msgid "Diaspora*"
msgstr "Diaspora*"

#: personal/personal/class_socialHandlers.inc:131
msgid "Diaspora accounts must look like user@pod"
msgstr "Diaspora accounts moeten eruitzien als gebruiker@pod"

#: personal/personal/class_socialHandlers.inc:141
msgid "LinkedIn"
msgstr "LinkedIn"

#: personal/personal/class_socialHandlers.inc:151
msgid "ORCID"
msgstr "ORCID"

#: personal/personal/class_socialHandlers.inc:164
msgid ""
"ORCID account IDs must look like XXXX-XXXX-XXXX-XXXX where X are digits"
msgstr ""
"ORCID account ID's moeten eruitzien als XXXX-XXXX-XXXX-XXXX waarbij X "
"cijfers zijn."
