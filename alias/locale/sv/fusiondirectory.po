# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:04+0000\n"
"PO-Revision-Date: 2018-08-13 19:49+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Swedish (https://www.transifex.com/fusiondirectory/teams/12202/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/alias/class_mailAliasDistribution.inc:26
#: admin/alias/class_mailAliasDistribution.inc:27
#: admin/alias/class_mailAliasDistribution.inc:30
msgid "Temporary mail distribution"
msgstr "Temporär epostdistribution"

#: admin/alias/class_mailAliasDistribution.inc:45
msgid "Mail distribution"
msgstr "Epost-distribution"

#: admin/alias/class_mailAliasDistribution.inc:48
#: admin/alias/class_mailAliasRedirection.inc:49
msgid "Name"
msgstr "Namn"

#: admin/alias/class_mailAliasDistribution.inc:48
msgid "Name to identify this alias"
msgstr "Namn för att att identifiera detta alias"

#: admin/alias/class_mailAliasDistribution.inc:50
#: admin/alias/class_mailAliasRedirection.inc:51
msgid "Description"
msgstr "Beskrivning"

#: admin/alias/class_mailAliasDistribution.inc:50
msgid "Description of this alias"
msgstr "Beskrivning av alias"

#: admin/alias/class_mailAliasDistribution.inc:54
msgid "Email address"
msgstr "Epostadress"

#: admin/alias/class_mailAliasDistribution.inc:59
msgid "Email aliases"
msgstr "Epostalias"

#: admin/alias/class_mailAliasDistribution.inc:59
msgid "Aliases for this email address"
msgstr "Alias för denna epostadress"

#: admin/alias/class_mailAliasDistribution.inc:64
msgid "Mail server"
msgstr "Epostserver"

#: admin/alias/class_mailAliasDistribution.inc:64
msgid "Mail server for this alias"
msgstr ""

#: admin/alias/class_mailAliasDistribution.inc:69
#: admin/alias/class_mailAliasRedirection.inc:67
msgid "Expiration date"
msgstr "Utgångsdatum"

#: admin/alias/class_mailAliasDistribution.inc:69
msgid ""
"Date after which the alias should be deleted. Leave empty for no deletion."
msgstr ""
"Datum efter vilket alias ska tas bort. Lämna tomt för att inte ta bort "
"aliaset."

#: admin/alias/class_mailAliasRedirection.inc:26
#: admin/alias/class_mailAliasRedirection.inc:27
#: admin/alias/class_mailAliasRedirection.inc:30
msgid "Temporary mail redirection"
msgstr "Temporär omdirigering för epost"

#: admin/alias/class_mailAliasRedirection.inc:46
msgid "Mail redirection"
msgstr "Epost-omdirigering"

#: admin/alias/class_mailAliasRedirection.inc:49
msgid "Name to identify this redirection"
msgstr "Namn för att identifiera omdirigeringen"

#: admin/alias/class_mailAliasRedirection.inc:51
msgid "Description of this redirection"
msgstr "Beskrivning av omdirigeringen"

#: admin/alias/class_mailAliasRedirection.inc:56
msgid "Redirect from"
msgstr "Omdirigera från"

#: admin/alias/class_mailAliasRedirection.inc:56
msgid "Mail address from which you want to redirect"
msgstr "Epostadress från vilken du vill omdirigera"

#: admin/alias/class_mailAliasRedirection.inc:62
msgid "Redirect to"
msgstr "Omdirigera till"

#: admin/alias/class_mailAliasRedirection.inc:62
msgid "Destination of this redirection"
msgstr "Destination för omdirigeringen"

#: admin/alias/class_mailAliasRedirection.inc:67
msgid ""
"Date after which the redirection should be deleted. Leave empty for no "
"deletion."
msgstr ""
"Datum då omdirigeringen ska tas bort. Lämna tomt för att inte ta bort."

#: admin/alias/class_aliasManagement.inc:37
msgid "Aliases"
msgstr "Alias"

#: admin/alias/class_aliasManagement.inc:38
msgid "Alias management"
msgstr "Alias-hantering"

#: admin/alias/class_aliasManagement.inc:39
msgid "Manage aliases"
msgstr ""

#: admin/alias/class_aliasManagement.inc:43
msgid "Mail aliases"
msgstr "Epost-alias"

#: config/alias/class_aliasConfig.inc:26
msgid "Alias configuration"
msgstr "Alias-konfigurering"

#: config/alias/class_aliasConfig.inc:27
msgid "FusionDirectory alias plugin configuration"
msgstr "FusionDirectory alias-plugin-konfiguration"

#: config/alias/class_aliasConfig.inc:40
msgid "Alias"
msgstr ""

#: config/alias/class_aliasConfig.inc:43
msgid "Alias RDN"
msgstr "Alias RDN"

#: config/alias/class_aliasConfig.inc:43
msgid "Branch in which aliases will be stored"
msgstr "Gren där alias ska lagras"
