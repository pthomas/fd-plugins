<?php
/*
  This code is part of FusionDirectory (http://www.fusiondirectory.org/)
  Copyright (C) 2005 Guillame Delecourt
  Copyright (C) 2006 Vincent Seynhaeve
  Copyright (C) 2006 Benoit Mortier
  Copyright (C) 2011-2016  FusionDirectory

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/*! \brief   nagios plugin

  This class provides the functionality to read and write all attributes
  relevant for nagiosAccount from/to the LDAP. It does syntax checking
  and displays the formulars required.
 */

class nagiosAccount extends simplePlugin
{
  var $displayHeader  = TRUE;

  static function plInfo (): array
  {
    return [
      'plShortName'   => _('Nagios'),
      'plDescription' => _('Nagios account settings'),
      'plFilter'      => '(objectClass=lconfContact)',
      'plIcon'        => 'geticon.php?context=applications&icon=nagios&size=48',
      'plSmallIcon'   => 'geticon.php?context=applications&icon=nagios&size=16',
      'plSelfModify'  => TRUE,
      'plPriority'    => 7,
      'plObjectClass' => ['lconfContact'],
      'plObjectType'  => ['user'],

      'plProvidedAcls'  => parent::generatePlProvidedAcls(static::getAttributesInfo())
    ];
  }

  /*!
   *  \brief The main function : information about attributes
   */
  static function getAttributesInfo (): array
  {
    $prefix = static::get_prefix();
    return [
      'main' => [
        'name'  => _('Nagios Account'),
        'attrs' => [
          new StringAttribute(
            _('Alias'), _('Name of the nagios alias'),
            $prefix.'Alias', TRUE
          ),
          new MailAttribute(
            _('Mail address'), _('Email of the nagios alias'),
            $prefix.'Email', TRUE
          ),
          new SelectAttribute(
            _('Host notification period'), _('Host notification period'),
            $prefix.'ContactHostNotificationPeriod', FALSE,
            ['24x7','24x5','8x5']
          ),
          new SelectAttribute(
            _('Service notification period'), _('Service notification period'),
            $prefix.'ContactServiceNotificationPeriod', FALSE,
            ['24x7','24x5','8x5']
          ),
          new SelectAttribute(
            _('Service notification options'), _('Service notification options'),
            $prefix.'ContactServiceNotificationOptions', FALSE,
            ['w,u,c,r,f','w,u,c,r','w,u,c','c,w','n']
          ),
          new SelectAttribute(
            _('Host notification options'), _('Host notification options'),
            $prefix.'ContactHostNotificationOptions', FALSE,
            ['d,u,r,f','d,u,r','d,u','n']
          ),
          new StringAttribute(
            _('Pager'), _($prefix.' Pager'),
            $prefix.'Pager', FALSE
          ),
          new StringAttribute(
            _('Service notification commands'), _('Service notification commands'),
            $prefix.'ContactServiceNotificationCommands', FALSE
          ),
          new StringAttribute(
            _('Host notification commands'), _('Host notification commands'),
            $prefix.'ContactHostNotificationCommands', FALSE
          )
        ]
      ]
    ];
  }

  static function get_prefix ()
  {
    global $config;
    return $config->get_cfg_value('lconfPrefix', 'lconf');
  }

  function __construct ($dn = NULL, $object = NULL, $parent = NULL, $mainTab = FALSE)
  {
    $prefix = static::get_prefix();
    $this->objectclasses = [$prefix.'Contact'];
    parent::__construct($dn, $object, $parent, $mainTab);
  }
}
