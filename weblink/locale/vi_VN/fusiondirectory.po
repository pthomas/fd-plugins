# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:05+0000\n"
"PO-Revision-Date: 2018-08-13 20:06+0000\n"
"Language-Team: Vietnamese (Viet Nam) (https://www.transifex.com/fusiondirectory/teams/12202/vi_VN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: vi_VN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: admin/systems/weblink/class_webLink.inc:30
msgid "Web link"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:31
msgid "Edit web link"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:43
#: admin/systems/weblink/class_webLink.inc:46
msgid "Links"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:46
msgid "Web links to this computer"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:52
msgid "Settings"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:55
msgid "Protocol"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:55
msgid "Protocol to use to access this computer Web page"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:61
msgid "Arbitrary links"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:61
msgid "Any URL you want to associate to this computer"
msgstr ""
